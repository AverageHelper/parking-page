# Parking Page

A simple HTML document that indicates my ownership of a web domain that I haven't got around to using yet.

## Usage

A workflow automatically publishes to pgs.sh directly using an SSH key known only to Forgejo. The contents of `public/` get uploaded directly.

### Custom Domain

To point a domain at this project, follow the [documentation at pico.sh](https://pico.sh/custom-domains#pgssh). At time of writing, [anyone may point a domain at this project](https://github.com/picosh/pico/issues/132), but you probably don't want to do that unless you mean to confuse people. (Telling people _I_ parked _your_ domain is very silly.)

The only canonical domains for this project are:

- `average.lgbt`
- `avg-parking-page.pgs.sh` (the default project domain)

Other domains that host this project are not mine.

## Contributing

Issues and pull requests are always welcome! You may sign in or create an account at [git.average.name](https://git.average.name) using your existing GitHub, GitLab, or Codeberg account via OAuth 2.0.
